export interface IUserRandom {
    gender: string;
    name: IUserName;
    location: IUserLocation;
    email: string;
    login: IUserAuth;
    dob: IUserDateAge;
    registered: IUserDateAge;
    phone: string;
    cell: string;
    picture: IUserPicture;
    nat: string;
    id: {
        name: string;
        value: string;
    };

}
export class UserRandom {
    gender: string;
    name: IUserName;
    location: IUserLocation;
    email: string;
    login: IUserAuth;
    dob: IUserDateAge;
    registered: IUserDateAge;
    phone: string;
    cell: string;
    picture: IUserPicture;
    nat: string;
    id: {
        name: string;
        value: string;
    };

}

export interface IUserName {
    title: string;
    first: string;
    last: string;
}

export interface IUserLocation {
    street: {
        number: number;
        name: string;
    };
    city: string;
    state: string;
    country: string;
    postcode: number;
    coordinates: {
        latitude: number;
        longitude: number;
    };
    timezone: {
        offset: number;
        description: string;
    };
}

export interface IUserAuth {
    uuid: string;
    username: string;
    password: string;
    salt: string;
    md5: string;
    sha1: string;
    sha256: string;
}

export interface IUserDateAge {
    date: string;
    age: number;
}

export interface IUserPicture {
    large: string;
    medium: string;
    thumbnail: string;
}
