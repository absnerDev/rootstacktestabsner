export interface IResponseUserRandom {
    results: Array<any>;
    info: {
        seed: string;
        results: number;
        page: number;
        version: number;
    };
}

export interface ISearchUser {
    results?: number;
    inc?: any;
    exc?: any;
    dob?: any;
    age?: any;
}
