import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IUserRandom, IUserName } from '../models/user.model';
import { IResponseUserRandom, ISearchUser } from '../models/response-api.model';
import { map, groupBy, toArray, mergeMap, tap, concatMap, repeatWhen, retry, repeat } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchToolsService {

  constructor(private readonly http: HttpClient) { }

  /**
   * fecthAndOrderUsers
   */
  public fecthAndOrderUsers(results?: number): Promise<IUserRandom[]> {
    const query: any = {
      params: {
        results
      }
    };
    return this.http.get<IUserRandom[]>(environment.API_RAMDOMUSER, query).pipe(
      map((response: any) => {
        const data: IUserRandom[] = response.results;
        data.sort((userA: IUserRandom, userB: IUserRandom) => (userA.name.first > userB.name.first) ? 1 : -1);
        return data;
      }),

    ).toPromise();
  }

  /**
   * fetchAndfindUsers
   */
  public fetchAndfindUsers(findAge: number, search?: ISearchUser): Promise<IUserRandom> {
    const params: any = {
      params: search
    };
    return this.http.get<IUserRandom>(environment.API_RAMDOMUSER, params).pipe(
      map((response: any) => {
        const users: IUserRandom[] = response.results;
        return users.find((user: IUserRandom) => user.dob.age === findAge + 1);
      })
    ).toPromise();
  }

  /**
   * fecthAndcount
   */
  public fecthAndcount(search?: ISearchUser): Promise<any> {
    const params: any = {
      params: search
    };
    return this.http.get<any>(environment.API_RAMDOMUSER, params).pipe(
      map((response: any) => {
        const users: IUserRandom[] = response.results;
        let allName = '';
        users.forEach((item: IUserRandom) => {
          allName = allName + (item.name.first + item.name.last);
        });
        allName = allName.toLocaleUpperCase();
        const unordered: any = this.countChart(allName);
        const ordered: Array<any> = [];
        Object.keys(unordered).forEach((key) => {
          ordered.push(
            {
              chart: key,
              count: unordered[key]
            }
          );
        });
        return {
          user: users,
          allNames: allName,
          ordered: ordered.sort((a, b) => (a.count > b.count) ? -1 : 1)
        };
      })
    ).toPromise();
  }

  /**
   * searchPlanetByTerrain
   */
  public searchPlanetByTerrain(url: string): Promise<any> {
    return this.http.get(url).pipe(
    ).toPromise();
  }

  private countChart(str: string): any {
    return [...str.replace(/\s/g, '')]
      .reduce((obj, char) => {
        obj[char] = obj[char] + 1 || 1;
        return obj;
      }, {});
  }

}
