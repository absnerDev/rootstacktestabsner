import { TestBed } from '@angular/core/testing';

import { SearchToolsService } from './search-tools.service';

describe('SearchToolsService', () => {
  let service: SearchToolsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchToolsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
