import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PagePracticesRoutingModule } from './page-practices-routing.module';
import { PagePracticesComponent } from './page-practices.component';
import { OrderComponent } from './order/order.component';
import { FindComponent } from './find/find.component';
import { CountComponent } from './count/count.component';
import { SearchToolsService } from '../shared/services/search-tools.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlanetsComponent } from './planets/planets.component';

@NgModule({
  declarations: [PagePracticesComponent, OrderComponent, FindComponent, CountComponent, PlanetsComponent],
  imports: [
    CommonModule,
    PagePracticesRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    FlexLayoutModule
  ],
  providers: [SearchToolsService]
})
export class PagePracticesModule { }
