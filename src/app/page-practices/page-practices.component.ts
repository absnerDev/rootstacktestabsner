import { Component, OnInit } from '@angular/core';
import { SearchToolsService } from '../shared/services/search-tools.service';
import { IUserRandom } from '../shared/models/user.model';
import { ISearchUser } from '../shared/models/response-api.model';
import { IPlanets, Planets } from '../shared/models/planets.model';
import { environment } from 'src/environments/environment';
import { ThemePalette } from '@angular/material/core';
import { from } from 'rxjs';

@Component({
  selector: 'app-page-practices',
  templateUrl: './page-practices.component.html',
  styleUrls: ['./page-practices.component.scss'],
  providers: [SearchToolsService]
})
export class PagePracticesComponent implements OnInit {

  public links: Array<INavbar> = [];
  public activeLink = this.links[0];
  public background: ThemePalette = undefined;


  constructor(private readonly searchToolsService: SearchToolsService) { }

  ngOnInit(): void {
    this.loadNavbar();
  }

  private loadNavbar(): void {
    this.links = [
      {
        path: 'order',
        name: 'Fetch & order'
      },
      {
        path: 'find',
        name: 'Fetch & find'
      },
      {
        path: 'count',
        name: 'Fetch & count'
      },
      {
        path: 'planets',
        name: 'Planet by terrain'
      }
    ];
  }

}

export interface INavbar {
  path: string;
  name: string;
}
