import { Component, OnInit } from '@angular/core';
import { ISearchUser } from 'src/app/shared/models/response-api.model';
import { SearchToolsService } from 'src/app/shared/services/search-tools.service';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.scss']
})
export class CountComponent implements OnInit {

  public data: any;

  constructor(private readonly searchToolsService: SearchToolsService) { }

  ngOnInit(): void {
    this.searchUserAndCountFullName();
  }

  private searchUserAndCountFullName(): void {
    const filter: ISearchUser = {
      results: 5
    };
    this.searchToolsService.fecthAndcount(filter).then((response: any) => {
      console.log('response count', response);
      this.data = response;
    }).catch((error) => {
      console.log('error', error);
    });
  }

}
