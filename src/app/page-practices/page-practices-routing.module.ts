import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagePracticesComponent } from './page-practices.component';
import { OrderComponent } from './order/order.component';
import { FindComponent } from './find/find.component';
import { CountComponent } from './count/count.component';
import { PlanetsComponent } from './planets/planets.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'order',
    pathMatch: 'full'
  },
  {
    path: '',
    component: PagePracticesComponent,
    children: [
      {
        path: 'order',
        component: OrderComponent
      },
      {
        path: 'find',
        component: FindComponent
      },
      {
        path: 'count',
        component: CountComponent
      },
      {
        path: 'planets',
        component: PlanetsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagePracticesRoutingModule { }
