import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ISearchUser } from 'src/app/shared/models/response-api.model';
import { SearchToolsService } from 'src/app/shared/services/search-tools.service';
import { IUserRandom, UserRandom } from 'src/app/shared/models/user.model';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {
  public value: FormControl = new FormControl(null);
  public user: IUserRandom;
  public loading: boolean;

  constructor(private readonly searchToolsService: SearchToolsService) { }

  ngOnInit(): void {
    this.searchUserByAge(25);
    this.value.valueChanges.pipe(
      debounceTime(1000),
      distinctUntilChanged()
    ).subscribe((value: number)  =>  {
      console.log('new age', value);
      this.searchUserByAge(value);
    });
  }

  private searchUserByAge(age: number): void {
    this.loading = false;
    const filter: ISearchUser = {
      results: 5000
    };
    this.searchToolsService.fetchAndfindUsers(age, filter).then((response: any) => {
      console.log('response age', response);
      this.user = response;
      this.loading = true;
    }).catch((error: any) => {
      console.log('error', error);
    });
  }

}
