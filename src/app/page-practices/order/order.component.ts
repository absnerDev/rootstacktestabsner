import { Component, OnInit } from '@angular/core';
import { SearchToolsService } from 'src/app/shared/services/search-tools.service';
import { IUserRandom } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  public listUser: Array<IUserRandom>;

  constructor(private readonly searchToolsService: SearchToolsService) { }

  ngOnInit(): void {
    this.searchUserByName();
  }

  private searchUserByName(): void {
    this.searchToolsService.fecthAndOrderUsers(10).then((user: IUserRandom[]) => {
      console.log('user', user);
      this.listUser = user;
    }).catch((error: any) => {
      console.log('error', error);
    });
  }

}
