import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePracticesComponent } from './page-practices.component';

describe('PagePracticesComponent', () => {
  let component: PagePracticesComponent;
  let fixture: ComponentFixture<PagePracticesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePracticesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePracticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
