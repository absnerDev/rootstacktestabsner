import { Component, OnInit } from '@angular/core';
import { IPlanets, Planets } from 'src/app/shared/models/planets.model';
import { environment } from 'src/environments/environment';
import { SearchToolsService } from 'src/app/shared/services/search-tools.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {
  public listPlanets: Array<any> = [];
  public findPlanet: IPlanets;
  public loading: boolean;
  public dataSelect: Array<string>;
  public optionValue: FormControl = new FormControl(null);

  constructor(private readonly searchToolsService: SearchToolsService) { }

  ngOnInit(): void {
    this.loadSelect();
    this.searchPlanet(`${environment.API_SWAPI}/planets/`);
    this.optionValue.valueChanges.subscribe((value: string) =>  {
      console.log('select', value);
      this.searchTerrain(this.listPlanets, value);
    });
  }

  private searchPlanet(url?: string): void {
    this.loading = false;
    this.searchToolsService.searchPlanetByTerrain(url).then((response: any) => {
      this.listPlanets.push(response.results);
      if (response.next) {
        this.searchPlanet(response.next);
      } else {
        const aux: Array<any> = [];
        this.listPlanets.forEach((item: Array<any>) => {
          item.forEach((planet: any) => {
            aux.push(planet);
          });
        });
        this.listPlanets = aux;
        this.searchTerrain(aux, 'jungle');
      }
    }).catch((error) => {
      console.log('error', error);
    });
  }

  private searchTerrain(list: Array<IPlanets>, terraint?: string): void {
    console.log('list planet', list);
    const filter: Array<IPlanets> = list.filter((item: IPlanets) => item.terrain.indexOf(terraint) >= 0);
    let aux: IPlanets = new Planets();
    aux.population = '0';
    filter.forEach((item: IPlanets) => {
      if (item.population !== 'unknown') {
        if (parseInt(aux.population, 10) < parseInt(item.population, 10)) {
          aux = item;
        }
      }
    });
    console.log('busqueda populacion', aux);
    this.loading = true;
    this.findPlanet = aux;
  }

  private loadSelect(): void {
    this.dataSelect = [
      'jungle',
      'rainforests',
      'tundra',
      'swamp',
      'jungles',
      'gas giant',
      'forests',
      'mountains',
      'lakes',
      'grassy hills',
      'cityscape',
      'ocean',
      'rock',
      'desert',
      'mountain',
      'barren',
      'scrublands',
      'savanna',
      'canyons',
      'sinkholes',
      'volcanoes',
      'lava rivers',
      'caves',
      'rivers',
      'airless asteroid',
      'glaciers',
      'ice canyons',
      'fungus forests',
      'fields',
      'rock arches',
      'grass',
      'plains',
      'urban',
      'hills',
      'oceans',
      'swamps',
      'bogs',
      'savannas',
      'grasslands',
      'rocky islands',
      'seas',
      'mesas',
      'deserts',
      'reefs',
      'islands'
    ]
  }

}
